Le interactive rebase allows one to tweak the commit history of current branch.


git checkout <myBranch>
git rebase -i HEAD~2         | HEAD~2 means "last two commits"


From there, the editor configure by default on your git instance will popup with a list of the commits asked.
A word prefixes each line. Instructions on what can be done are then showed in the editor :

-pick, to pick commit as this
-r for reword, to reword comment
-s for sauash, to merge selected commit with previous one
-...
